
var pluginName = 'scanimetrics-heatmap-panel',
	axesEditor = 'public/plugins/'+ pluginName +'/paneltab_Axes.html',
	legendEditor = 'public/plugins/'+ pluginName +'/paneltab_Legend.html',
	displayEditor = 'public/plugins/'+ pluginName +'/paneltab_Display.html';

export {
	pluginName,
	axesEditor,
	legendEditor,
	displayEditor
}