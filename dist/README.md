# scanimetrics-heatmap-panel

A Plot.ly Heatmap Panel Plugin for Grafana

This grafana plugin began life as a fork of the Savantly grafana-heatmap plugin.  That plugin provided help in learning the grafana plugin environment and using D3 and D3Plus.  A warm thank you to the Savantly-net team for helping me to get started.

As I worked with D3Plus, I was unable to get the results I was looking for and turned my attention to Plot.ly

Example  
![Heatmap](https://bytebucket.org/scanimetrics/scanimetrics-heatmap-panel/raw/a67ae51fe5e7f018d5764d56526c8a8ccc661794/src/img/heatmap-panel.png)

Legend Options  
![Legend](https://bytebucket.org/scanimetrics/scanimetrics-heatmap-panel/raw/a67ae51fe5e7f018d5764d56526c8a8ccc661794/src/img/heatmap-legend-tab.png)

Display Options  
![Display](https://bytebucket.org/scanimetrics/scanimetrics-heatmap-panel/raw/a67ae51fe5e7f018d5764d56526c8a8ccc661794/src/img/heatmap-display-tab.png)
