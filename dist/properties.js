'use strict';

System.register([], function (_export, _context) {
	"use strict";

	var pluginName, axesEditor, legendEditor, displayEditor;
	return {
		setters: [],
		execute: function () {
			_export('pluginName', pluginName = 'scanimetrics-heatmap-panel');

			_export('axesEditor', axesEditor = 'public/plugins/' + pluginName + '/paneltab_Axes.html');

			_export('legendEditor', legendEditor = 'public/plugins/' + pluginName + '/paneltab_Legend.html');

			_export('displayEditor', displayEditor = 'public/plugins/' + pluginName + '/paneltab_Display.html');

			_export('pluginName', pluginName);

			_export('axesEditor', axesEditor);

			_export('legendEditor', legendEditor);

			_export('displayEditor', displayEditor);
		}
	};
});
//# sourceMappingURL=properties.js.map
