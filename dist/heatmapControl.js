'use strict';

System.register(['app/core/time_series2', 'app/core/utils/kbn', 'app/plugins/sdk', './properties', 'lodash', 'moment', './series_overrides_heatmap_ctrl', './css/heatmap.css!'], function (_export, _context) {
	"use strict";

	var TimeSeries, kbn, MetricsPanelCtrl, axesEditor, legendEditor, displayEditor, pluginName, _, moment, _createClass, panelOptions, panelDefaults, HeatmapCtrl;

	function _toConsumableArray(arr) {
		if (Array.isArray(arr)) {
			for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
				arr2[i] = arr[i];
			}

			return arr2;
		} else {
			return Array.from(arr);
		}
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	function _possibleConstructorReturn(self, call) {
		if (!self) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}

		return call && (typeof call === "object" || typeof call === "function") ? call : self;
	}

	function _inherits(subClass, superClass) {
		if (typeof superClass !== "function" && superClass !== null) {
			throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
		}

		subClass.prototype = Object.create(superClass && superClass.prototype, {
			constructor: {
				value: subClass,
				enumerable: false,
				writable: true,
				configurable: true
			}
		});
		if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	}

	function ensureArrayContains(array, value) {
		if (array.indexOf(value) == -1) {
			array.push(value);
		}
	}

	function colorToHex(color) {
		if (color.substr(0, 1) === '#') {
			return color;
		}
		var digits = color.replace(/[rgba\(\)\ ]/g, '').split(',');
		while (digits.length < 3) {
			digits.push(255);
		}

		var red = parseInt(digits[0]);
		var green = parseInt(digits[1]);
		var blue = parseInt(digits[2]);

		var rgba = blue | green << 8 | red << 16;
		return '#' + rgba.toString(16);
	}

	function getColorByXPercentage(canvas, xPercent) {
		var x = canvas.width * xPercent || 0;
		var context = canvas.getContext("2d");
		var p = context.getImageData(x, 1, 1, 1).data;
		var color = 'rgba(' + [p[0] + ',' + p[1] + ',' + p[2] + ',' + p[3]] + ')';
		return color;
	}

	return {
		setters: [function (_appCoreTime_series) {
			TimeSeries = _appCoreTime_series.default;
		}, function (_appCoreUtilsKbn) {
			kbn = _appCoreUtilsKbn.default;
		}, function (_appPluginsSdk) {
			MetricsPanelCtrl = _appPluginsSdk.MetricsPanelCtrl;
		}, function (_properties) {
			axesEditor = _properties.axesEditor;
			legendEditor = _properties.legendEditor;
			displayEditor = _properties.displayEditor;
			pluginName = _properties.pluginName;
		}, function (_lodash) {
			_ = _lodash.default;
		}, function (_moment) {
			moment = _moment.default;
		}, function (_series_overrides_heatmap_ctrl) {}, function (_cssHeatmapCss) {}],
		execute: function () {
			_createClass = function () {
				function defineProperties(target, props) {
					for (var i = 0; i < props.length; i++) {
						var descriptor = props[i];
						descriptor.enumerable = descriptor.enumerable || false;
						descriptor.configurable = true;
						if ("value" in descriptor) descriptor.writable = true;
						Object.defineProperty(target, descriptor.key, descriptor);
					}
				}

				return function (Constructor, protoProps, staticProps) {
					if (protoProps) defineProperties(Constructor.prototype, protoProps);
					if (staticProps) defineProperties(Constructor, staticProps);
					return Constructor;
				};
			}();

			panelOptions = {
				aggregationFunctions: ['avg', 'min', 'max', 'total', 'current', 'count'],
				scale: {
					position: ['left', 'right', 'top', 'bottom']
				},
				heatMap: {
					aggregationFunctions: ['sum', 'min', 'max', 'extent', 'mean', 'median', 'quantile', 'variance', 'deviation'],
					timestampFormats: ['YYYY-MM-DDTHH', 'YYYY-MM-DDTHH:mm', 'YYYY-MM-DDTHH:mm:ss', 'YYYY-MM-DDTHH:mm:ss.sssZ'],
					seriesMetadata: ['none', 'metric name', 'tag', 'custom']
				}
			};
			panelDefaults = {
				// other style overrides
				seriesOverrides: [],
				thresholdtext: '0,10',
				thresholds: [0, 10],
				colors: ['rgba(50, 172, 45, 1)', 'rgba(241, 255, 0, 1)', 'rgba(245, 54, 54, 1)'],
				legend: {
					show: true,
					min: true,
					max: true,
					avg: true,
					current: true,
					total: true,
					position: 'right',
					hideEmpty: false,
					hideZero: false
				},
				seriesDisplayDecimals: 2,
				maxDataPoints: 100,
				mappingType: 1,
				nullPointMode: 'connected',
				format: 'none',
				valueMaps: [{ value: 'null', op: '=', text: 'N/A' }],
				heatMap: {
					show: true,
					enableGrouping: true,
					row: {
						name: 'Row',
						regex: '/y[0-9][0-9]?/i',
						rangetext: '1,9',
						range: [1, 9],
						plotrange: [0.5, 9.5],
						titlefont: {
							size: 12
						},
						tickfont: {
							size: 12
						}
					},
					column: {
						name: 'Column',
						regex: '/x[0-9][0-9]?/i',
						rangetext: '0,13',
						range: [0, 13],
						plotrange: [-0.5, 13.5],
						titlefont: {
							size: 12
						},
						tickfont: {
							size: 12
						}
					},
					scale: {
						show: true,
						position: 'right',
						text: true,
						title: 'Title',
						ticktext: '0,10',
						ticks: [0, 10],
						colorscale: [[0, 'rgba(220,0,0,1)'], [0.33, 'rgba(240,240,0,1)'], [0.5, 'rgba(0,240,0,1)'], [0.67, 'rgba(0,240,240,1)'], [1, 'rgba(0,0,220,1)']],
						titlefont: {
							size: 12
						},
						tickfont: {
							size: 12
						}
					},
					valueText: true,
					valueScale: 1.0,
					unitText: '',
					valuePrecision: 1,
					valueFont: {
						size: 12
					},
					debug: false,
					smooth: true,
					aggregationFunction: 'mean',
					colorByFunction: 'max',
					sizeByFunction: 'count',
					seriesMetadata: 'none',
					ids: ['gridY']
				}
			};

			_export('MetricsPanelCtrl', _export('HeatmapCtrl', HeatmapCtrl = function (_MetricsPanelCtrl) {
				_inherits(HeatmapCtrl, _MetricsPanelCtrl);

				function HeatmapCtrl($scope, $injector, $sce) {
					_classCallCheck(this, HeatmapCtrl);

					var _this2 = _possibleConstructorReturn(this, (HeatmapCtrl.__proto__ || Object.getPrototypeOf(HeatmapCtrl)).call(this, $scope, $injector));

					_.defaults(_this2.panel, panelDefaults);

					_this2.options = panelOptions;
					_this2.panel.chartId = 'chart_' + _this2.panel.id;
					_this2.containerDivId = 'container_' + _this2.panel.chartId;
					_this2.$sce = $sce;
					_this2.events.on('init-edit-mode', _this2.onInitEditMode.bind(_this2));
					_this2.events.on('data-received', _this2.onDataReceived.bind(_this2));
					_this2.events.on('data-snapshot-load', _this2.onDataReceived.bind(_this2));
					_this2.initializePanel();
					return _this2;
				}

				_createClass(HeatmapCtrl, [{
					key: 'initializePanel',
					value: function initializePanel() {
						var _this = this;
						var meta = {};

						var plotlyPath = 'plugins/' + pluginName + '/libs/plotly.js/dist/plotly.js';
						meta[plotlyPath] = { format: 'global' };

						SystemJS.config({ meta: meta });

						SystemJS.import(plotlyPath).then(function plotlyLoaded() {
							console.log('plotly is loaded');
							//			_this.events.emit('data-received');
						});
					}
				}, {
					key: 'handleError',
					value: function handleError(err) {
						this.getPanelContainer().html('<p>Error:</p><pre>' + err + '</pre>');
					}
				}, {
					key: 'onInitEditMode',
					value: function onInitEditMode() {
						this.addEditorTab('Axes', axesEditor, 2);
						this.addEditorTab('Legend', legendEditor, 3);
						this.addEditorTab('Display', displayEditor, 4);
					}
				}, {
					key: 'getPanelContainer',
					value: function getPanelContainer() {
						return $(document.getElementById(this.containerDivId));
					}
				}, {
					key: 'onDataReceived',
					value: function onDataReceived(dataList) {
						console.info('received data');
						console.debug(dataList);
						if (typeof dataList !== 'undefined') {
							this.series = dataList.map(this.seriesHandler.bind(this));
							console.info('mapped dataList to series');
							var preparedData = this.dataProcessor(this.series);
							this.render(preparedData);
						}
					}
				}, {
					key: 'dataProcessor',
					value: function dataProcessor(dataArray) {
						var resultArray = [];
						for (var dataIndex = 0; dataIndex < dataArray.length; dataIndex++) {
							var newDataItem = Object.assign({}, dataArray[dataIndex]);
							Object.assign(newDataItem, dataArray[dataIndex].stats);
							delete newDataItem.stats;

							var regex = kbn.stringToJsRegex(this.panel.heatMap.column.regex);
							var matches = newDataItem.alias.match(regex);
							if (matches && matches.length > 0) {
								newDataItem.column = matches[0];
							} else {
								newDataItem.column = 'NA';
							}
							var regex = kbn.stringToJsRegex(this.panel.heatMap.row.regex);
							var matches = newDataItem.alias.match(regex);
							if (matches && matches.length > 0) {
								newDataItem.row = matches[0];
							} else {
								newDataItem.row = 'NA';
							}

							resultArray.push(newDataItem);
						}

						var heatMapTrace = { x: [], y: [], z: [] };
						var y0 = parseInt(this.panel.heatMap.row.range[0]);
						if (this.panel.heatMap.row.range[1] >= this.panel.heatMap.row.range[0]) {
							for (var i = this.panel.heatMap.row.range[0]; i < this.panel.heatMap.row.range[1] + 1; i++) {
								heatMapTrace.z.push([]);
							}
						} else {
							y0 = parseInt(this.panel.heatMap.row.range[1]);
							for (var i = this.panel.heatMap.row.range[1]; i < this.panel.heatMap.row.range[0] + 1; i++) {
								heatMapTrace.z.push([]);
							}
						}
						for (var dataIndex = 0; dataIndex < resultArray.length; dataIndex++) {
							var x = parseInt(resultArray[dataIndex].column.substring(1));
							var y = parseInt(resultArray[dataIndex].row.substring(1));
							var z = resultArray[dataIndex].flotpairs[0][1];
							if (!isNaN(x) && !isNaN(y)) {
								if (this.checkRange(x, this.panel.heatMap.column.range) && this.checkRange(y, this.panel.heatMap.row.range)) {
									heatMapTrace.z[y - y0][x] = z;
								}
							}
						}
						resultArray = [heatMapTrace];
						return resultArray;
					}
				}, {
					key: 'seriesHandler',
					value: function seriesHandler(seriesData) {
						var series = new TimeSeries({
							datapoints: seriesData.datapoints,
							alias: seriesData.target.replace(/"|,|;|=|:|{|}/g, '_')
						});
						series.flotpairs = series.getFlotPairs(this.panel.nullPointMode);
						return series;
					}
				}, {
					key: 'addSeriesOverride',
					value: function addSeriesOverride(override) {
						this.panel.seriesOverrides.push(override || {});
					}
				}, {
					key: 'removeSeriesOverride',
					value: function removeSeriesOverride(override) {
						this.panel.seriesOverrides = _.without(this.panel.seriesOverrides, override);
						this.render();
					}
				}, {
					key: 'updateThresholds',
					value: function updateThresholds() {
						var thresholdCount = this.panel.thresholds.length;
						var colorCount = this.panel.colors.length;
						this.panel.thresholds = this.panel.thresholdtext.split(',').map(function (strVale) {
							return Number(strVale.trim());
						});
						this.refresh();
					}
				}, {
					key: 'updateColumnRange',
					value: function updateColumnRange() {
						this.panel.heatMap.column.range = this.panel.heatMap.column.rangetext.split(',').map(function (strVale) {
							return Number(strVale.trim());
						});
						this.panel.heatMap.column.plotrange = this.panel.heatMap.column.range;
						if (this.panel.heatMap.column.range[0] < this.panel.heatMap.column.range[1]) {
							this.panel.heatMap.column.plotrange[0] -= 0.5;
							this.panel.heatMap.column.plotrange[1] += 0.5;
						} else {
							this.panel.heatMap.column.plotrange[0] += 0.5;
							this.panel.heatMap.column.plotrange[1] -= 0.5;
						}
					}
				}, {
					key: 'updateRowRange',
					value: function updateRowRange() {
						this.panel.heatMap.row.range = this.panel.heatMap.row.rangetext.split(',').map(function (strVale) {
							return Number(strVale.trim());
						});
						this.panel.heatMap.row.plotrange = this.panel.heatMap.row.range;
						if (this.panel.heatMap.row.range[0] < this.panel.heatMap.row.range[1]) {
							this.panel.heatMap.row.plotrange[0] -= 0.5;
							this.panel.heatMap.row.plotrange[1] += 0.5;
						} else {
							this.panel.heatMap.row.plotrange[0] += 0.5;
							this.panel.heatMap.row.plotrange[1] -= 0.5;
						}
					}
				}, {
					key: 'checkRange',
					value: function checkRange(val, x) {
						if (x[0] < x[1]) {
							var test = val >= x[0];
							var test = val <= x[1];
							return val >= x[0] && val <= x[1];
						} else {
							var test = val >= x[1];
							var test = val <= x[0];
							return val >= x[1] && val <= x[0];
						}
					}
				}, {
					key: 'changeColor',
					value: function changeColor(colorIndex, color) {
						this.panel.colors[colorIndex] = color;
					}
				}, {
					key: 'removeColor',
					value: function removeColor(colorIndex) {
						this.panel.colors.splice(colorIndex, 1);
					}
				}, {
					key: 'addColor',
					value: function addColor() {
						this.panel.colors.push('rgba(255, 255, 255, 1)');
					}
				}, {
					key: 'addColorToScale',
					value: function addColorToScale() {
						this.panel.heatMap.scale.colorscale.push([1, 'rgba(255, 255, 255, 1)']);
						this.refresh();
					}
				}, {
					key: 'removeColorFromScale',
					value: function removeColorFromScale(colorIndex) {
						this.panel.heatMap.scale.colorscale.splice(colorIndex, 1);
						this.refresh();
					}
				}, {
					key: 'changeColorScale',
					value: function changeColorScale(colorIndex, color) {
						this.panel.heatMap.scale.colorscale[colorIndex][1] = color;
						this.refresh();
					}
				}, {
					key: 'invertColorScaleOrder',
					value: function invertColorScaleOrder() {
						var x = [];
						var len = this.panel.heatMap.scale.colorscale.length;
						for (var i = 0; i < len; i++) {
							x.push([this.panel.heatMap.scale.colorscale[i][0], this.panel.heatMap.scale.colorscale[len - i - 1][1]]);
						}
						this.panel.heatMap.scale.colorscale = x;
						this.refresh();
					}
				}, {
					key: 'updateColorScaleTicks',
					value: function updateColorScaleTicks() {
						this.panel.heatMap.scale.ticks = this.panel.heatMap.scale.ticktext.split(',').map(function (strVale) {
							return Number(strVale.trim());
						});
						this.refresh();
					}
				}, {
					key: 'getGradientForValue',
					value: function getGradientForValue(data, value) {
						var min = Math.min.apply(Math, data.thresholds);
						var max = Math.max.apply(Math, data.thresholds);
						var absoluteDistance = max - min;
						var valueDistanceFromMin = value - min;
						var xPercent = valueDistanceFromMin / absoluteDistance;
						// Get the smaller number to clamp at 0.99 max
						xPercent = Math.min(0.99, xPercent);
						// Get the larger number to clamp at 0.01 min
						xPercent = Math.max(0.01, xPercent);

						return getColorByXPercentage(this.canvas, xPercent);
					}
				}, {
					key: 'applyOverrides',
					value: function applyOverrides(seriesItemAlias) {
						var seriesItem = {},
						    colorData = {},
						    overrides = {};
						console.info('applying overrides for seriesItem');
						console.debug(seriesItemAlias);
						console.debug(this.panel.seriesOverrides);
						for (var i = 0; i <= this.panel.seriesOverrides.length; i++) {
							console.debug('comparing:');
							console.debug(this.panel.seriesOverrides[i]);
							if (this.panel.seriesOverrides[i] && this.panel.seriesOverrides[i].alias == seriesItemAlias) {
								overrides = this.panel.seriesOverrides[i];
							}
						}
						colorData.thresholds = (overrides.thresholds || this.panel.thresholdtext).split(',').map(function (strVale) {
							return Number(strVale.trim());
						});
						colorData.colorMap = this.panel.colors;
						seriesItem.colorData = colorData;

						seriesItem.valueName = overrides.valueName || this.panel.valueName;

						return seriesItem;
					}
				}, {
					key: 'invertColorOrder',
					value: function invertColorOrder() {
						this.panel.colors.reverse();
						this.refresh();
					}
				}, {
					key: 'link',
					value: function link(scope, elem, attrs, ctrl) {
						var chartElement = elem.find('.heatmap');
						chartElement.append('<div id="' + ctrl.containerDivId + '"></div>');
						var chartContainer = $(document.getElementById(ctrl.containerDivId));
						console.debug('found chartContainer');
						console.debug(chartContainer);
						elem.css('height', ctrl.height + 'px');

						var canvas = elem.find('.canvas')[0];
						ctrl.canvas = canvas;
						var gradientValueMax = elem.find('.gradient-value-max')[0];
						var gradientValueMin = elem.find('.gradient-value-min')[0];

						function render(data) {
							updateSize();
							updateCanvasStyle();
							updateChart(data);
						}

						function redraw() {
							var update = {
								width: chartElement[0].clientWidth
							};
							Plotly.relayout(ctrl.containerDivId, update);
						}

						function updateCanvasStyle() {
							canvas.width = Math.max(chartElement[0].clientWidth, 100);
							var canvasContext = canvas.getContext("2d");
							canvasContext.clearRect(0, 0, canvas.width, canvas.height);
							ctrl.canvasContext = canvasContext;
						}

						function updateSize() {
							elem.css('height', ctrl.height + 'px');
						}

						function updateChart(data) {
							// Make sure the necessary IDs are added
							var idKeys = Array.from(ctrl.panel.heatMap.ids);
							if (idKeys.length == 0) {
								ensureArrayContains(idKeys, 'alias');
							}

							// Setup Aggregations 
							var aggs = {};
							aggs.value = ctrl.panel.heatMap.aggregationFunction;
							aggs.current = ctrl.panel.heatMap.aggregationFunction;
							aggs.count = 'sum';
							aggs.total = 'sum';
							aggs.avg = 'mean';
							aggs.min = 'min';
							aggs.max = 'max';

							// Data for debug
							var plotlyTrace = [{
								x: ['x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10'],
								y: ['y0', 'y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7', 'y8', 'y9', 'y10'],
								z: [[, 0.185, 0.204, 0.200, 0.124, 0.184, 0.186, 0.120, 0.190, 0.192], [, 0.184, 0.208, 0.196, 0.128, 0.175, 0.174, 0.124, 0.172, 0.171], [, 0.183, 0.212, 0.192, 0.132, 0.167, 0.163, 0.128, 0.142, 0.151], [, 0.182, 0.216, 0.188, 0.136, 0.160, 0.190, 0.132, 0.135, 0.141], [, 0.181, 0.220, 0.184, 0.140, 0.110, 0.150, 0.136, 0.133, 0.131], [, 0.180, 0.224, 0.180, 0.144, 0.155, 0.140, 0.140, 0.135], [, 0.179, 0.228, 0.176, 0.148, 0.157, 0.135,,,], [, 0.178, 0.232, 0.172, 0.152, 0.150,,,,], [, 0.177, 0.236, 0.168, 0.156, 0.140,,,,], [, 0.176, 0.240, 0.164, 0.160,,,,,]]
							}];

							if (ctrl.panel.heatMap.debug) {
								data = plotlyTrace;
							}

							//  Set up the X Axis
							var plotlyXaxisOptions = {
								title: ctrl.panel.heatMap.column.name,
								side: 'top',
								range: ctrl.panel.heatMap.column.plotrange,
								showgrid: false,
								zeroline: false,
								linecolor: 'black',
								showticklabels: true,
								ticks: '',
								titlefont: ctrl.panel.heatMap.column.titlefont,
								tickfont: ctrl.panel.heatMap.column.tickfont
							};

							//  Set up the Y axis
							var plotlyYaxisOptions = {
								title: ctrl.panel.heatMap.row.name,
								side: 'left',
								range: ctrl.panel.heatMap.row.plotrange,
								showgrid: false,
								zeroline: false,
								linecolor: 'black',
								showticklabels: true,
								ticks: '',
								titlefont: ctrl.panel.heatMap.row.titlefont,
								tickfont: ctrl.panel.heatMap.row.tickfont
							};

							//  Set up the heatmap layout options
							var heatMapPlotlyLayout = {
								title: '',
								height: ctrl.height,
								xaxis: plotlyXaxisOptions,
								yaxis: plotlyYaxisOptions,
								paper_bgcolor: 'black',
								plot_bgcolor: 'black',
								font: ctrl.panel.heatMap.valueFont
							};

							if (ctrl.panel.heatMap.valueText) {
								heatMapPlotlyLayout.annotations = data[0].z.map(function (row, i) {
									return row.map(function (item, j) {
										return { x: j, y: i, text: (item * ctrl.panel.heatMap.valueScale).toFixed(ctrl.panel.heatMap.valuePrecision) + ctrl.panel.heatMap.unitText, showarrow: false };
									});
								}).reduce(function (acc, cur) {
									return acc.concat(cur);
								}, []);
							}

							//  Hover text			
							var text = data[0].z.map(function (row, i) {
								return row.map(function (item, j) {
									return 'X: ' + j + '<br>Y: ' + i + '<br>Value: ' + (item * ctrl.panel.heatMap.valueScale).toFixed(ctrl.panel.heatMap.valuePrecision) + ctrl.panel.heatMap.unitText;
								});
							});

							var colorbarOptions = {
								title: ctrl.panel.heatMap.scale.title,
								titleside: 'right',
								showticklabels: ctrl.panel.heatMap.scale.text,
								autotick: false,
								nticks: ctrl.panel.heatMap.scale.ticks.length,
								tick0: Math.min.apply(Math, _toConsumableArray(ctrl.panel.heatMap.scale.ticks)),
								dtick: (Math.max.apply(Math, _toConsumableArray(ctrl.panel.heatMap.scale.ticks)) - Math.min.apply(Math, _toConsumableArray(ctrl.panel.heatMap.scale.ticks))) / (ctrl.panel.heatMap.scale.ticks.length - 1),
								tickfont: ctrl.panel.heatMap.scale.tickfont,
								titlefont: ctrl.panel.heatMap.scale.titlefont
							};

							var heatMapPlotlyOptions = {
								type: 'heatmap',
								colorscale: ctrl.panel.heatMap.scale.colorscale,
								showscale: ctrl.panel.heatMap.scale.show,
								zmax: Math.max.apply(Math, _toConsumableArray(ctrl.panel.heatMap.scale.ticks)),
								zmin: Math.min.apply(Math, _toConsumableArray(ctrl.panel.heatMap.scale.ticks)),
								zsmooth: 'best',
								connectgaps: true,
								text: text,
								hoverinfo: 'text',
								colorbar: colorbarOptions
							};

							var modeBarOptions = {
								displaylogo: false,
								displayModeBar: false,
								staticPlot: false
							};

							var plotlyAnnotations = {};

							if (ctrl.panel.heatMap.smooth) {
								heatMapPlotlyOptions.zsmooth = 'best';
							} else {
								heatMapPlotlyOptions.zsmooth = false;
							}

							if (ctrl.panel.nullPointMode === 'connected') {
								heatMapPlotlyOptions.connectgaps = true;
							} else {
								heatMapPlotlyOptions.connectgaps = false;
							}

							Object.assign(data[0], heatMapPlotlyOptions);
							Plotly.newPlot(ctrl.containerDivId, data, heatMapPlotlyLayout, modeBarOptions);
						}

						this.events.on('render', function onRender(data) {
							var callback = function callback() {
								if (typeof Plotly !== 'undefined') {
									if (data) {
										console.info('Render Event:  New Plot');
										render(data);
										ctrl.renderingCompleted();
									} else {
										console.info('Render Event:  Redraw');
										redraw();
										ctrl.renderingCompleted();
									}
								} else {
									console.info('Render Event:  Plotly is not loaded yet');
									setTimeout(callback, 1000);
								}
							};
							callback();
						});
					}
				}]);

				return HeatmapCtrl;
			}(MetricsPanelCtrl)));

			;HeatmapCtrl.templateUrl = 'module.html';

			_export('HeatmapCtrl', HeatmapCtrl);

			_export('MetricsPanelCtrl', HeatmapCtrl);
		}
	};
});
//# sourceMappingURL=heatmapControl.js.map
